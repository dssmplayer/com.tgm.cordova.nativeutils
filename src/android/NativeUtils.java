package com.tgm.cordova.nativeutils;

import android.annotation.TargetApi;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.media.AudioManager;
import android.content.Context;
import android.graphics.Color;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaArgs;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaResourceApi;
import org.apache.cordova.PluginResult;
import org.apache.cordova.CordovaWebViewEngine;
import org.apache.cordova.engine.SystemWebView;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CordovaInterface;
import org.json.JSONException;
import org.json.JSONObject;

import com.topjohnwu.superuser.Shell;
import org.json.JSONArray;
import org.json.JSONException;
import java.util.List;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.BufferedOutputStream;
import java.io.BufferedInputStream;
import java.lang.StringBuffer;
import java.lang.Process;
import java.io.DataOutputStream;

import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

import java.io.File;
import android.view.View;

import com.google.android.things.pio.Gpio;
//import com.google.android.things.pio.PeripheralManagerService;
import com.google.android.things.pio.PeripheralManager;

import android.provider.Settings;

public class NativeUtils extends CordovaPlugin{

    protected static final String LOG_TAG = "NativeUtils";

    protected static final String ASSETS = "/android_asset/";

    private CallbackContext callbackContext = null;
	private CordovaWebView webView;
	private boolean isMonitoring = true;

	static{
		//Shell.enableVerboseLogging = BuildConfig.DEBUG;
		Shell.enableVerboseLogging = true;
		Shell.setDefaultBuilder(Shell.Builder.create()
				.setFlags(Shell.FLAG_REDIRECT_STDERR)
				.setTimeout(10)
				);
	}

	@Override
	public void initialize(final CordovaInterface cordova, CordovaWebView webView) {
		Log.d(LOG_TAG, "set viewport");
		this.webView = webView;
		super.initialize(cordova, webView);
		Activity activity = this.cordova.getActivity();
		webView.getView().setBackgroundColor(Color.TRANSPARENT);	// set background of view to transparent

/*
		Shell.getShell(shell -> {
			// The main shell is now constructed and cached
			// Exit splash screen and enter main activity
//			Intent intent = new Intent(activity, com.tgm.connectplayer.debug.MainActivity.class);
//			activity.startActivity(intent);
//			activity.finish();
		});
*/
	}
    /**
     * Executes the request and returns PluginResult.
     *
     * @param action        The action to execute.
     * @param args          JSONArray of arguments for the plugin.
     * @param callbackId    The callback id used when calling back into JavaScript.
     * @return              A PluginResult object with a status and message.
     */
    public boolean execute(String action, CordovaArgs args, CallbackContext callbackContext) throws JSONException {
		Log.d(LOG_TAG, "command: " + action);
		if (action.equals("fileExists")) {
			String path = args.getString(0);
			if(new File(path).exists()) {
				callbackContext.success("File exists");
			}else{
				callbackContext.error("File not exists");
			}
            return true;
		}else if(action.equals("exitApp")){
			//cordova.getActivity().finish();
			cordova.getActivity().finishAffinity();
			System.runFinalizersOnExit(true);
			System.exit(0);
			return true;
   		}else if(action.equals("setMinimumFontSize")){
			cordova.getActivity().runOnUiThread(new Runnable() {
				public void run(){
					int minSize = 0;
					try {
						minSize = args.getInt(0);
					} catch (JSONException e) {}
					CordovaWebViewEngine engine = webView.getEngine();
					SystemWebView webView = (SystemWebView)engine.getView();
					webView.getSettings().setMinimumFontSize(minSize);

					callbackContext.success("OK");
				}
			});
			return true;
		}else if(action.equals("audioOff")){
			cordova.getActivity().runOnUiThread(new Runnable() {
				public void run(){
					AudioManager audioManager = (AudioManager)cordova.getActivity().getSystemService(Context.AUDIO_SERVICE);
					audioManager.setStreamMute(AudioManager.STREAM_MUSIC, true);
					callbackContext.success("OK");
				}
			});
			return true;
		}else if(action.equals("audioOn")){
			cordova.getActivity().runOnUiThread(new Runnable() {
				public void run(){
					AudioManager audioManager = (AudioManager)cordova.getActivity().getSystemService(Context.AUDIO_SERVICE);
					audioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
					callbackContext.success("OK");
				}
			});
			return true;
		}else if(action.equals("setAudioVolume")){
			final JSONObject options = args.getJSONObject(0);
			final int volumePercent = options.getInt("volumePercent");
			cordova.getActivity().runOnUiThread(new Runnable() {
				public void run(){
					setAudioVolume(volumePercent);
					callbackContext.success("OK");
				}
			});
			return true;
		}else if(action.equals("suRun")){
			final String command = args.getString(0);
			cordova.getThreadPool().execute(new Runnable() {
				@Override
				public void run() {
					if(command == null || command.equals("")) {
						callbackContext.error("Invalid command supplied");
						return;
					}

					//NativeUtils.this.execCommand(command, callbackContext);
					NativeUtils.this.suShell(command, callbackContext);
					//NativeUtils.this.cmdShell(command, callbackContext);
				}
			});
			return true;
		}else if(action.equals("cmdRun")){
			final String command = args.getString(0);
			cordova.getThreadPool().execute(new Runnable() {
				@Override
				public void run() {
					if(command == null || command.equals("")) {
						callbackContext.error("Invalid command supplied");
						return;
					}

					//NativeUtils.this.execCommand(command, callbackContext);
					NativeUtils.this.cmdShell(command, callbackContext);
					//NativeUtils.this.suShell(command, callbackContext);
				}
			});
			return true;
		}else if(action.equals("getMacAddress")){
			final String inf = args.getString(0);

			cordova.getThreadPool().execute(new Runnable() {
				@Override
				public void run() {
					String __inf = inf;
					if(__inf == null || __inf.equals(""))
						__inf = "eth0";

					try{
						final String mac = getMacAddress(__inf);
						callbackContext.success(mac);
					}catch (Exception e){
						callbackContext.error("cannot get MAC address of " + inf);
					}
					return;
				}
			});
			return true;
		}else if(action.equals("getIpAddress")){
			final String inf = args.getString(0);

			cordova.getThreadPool().execute(new Runnable() {
				@Override
				public void run() {
					String __inf = inf;
					if(__inf == null || __inf.equals(""))
						__inf = "eth0";

					try{
						final String ip = getIPv4Address(__inf);
						Log.d(LOG_TAG, "get ip address: " + __inf + ":" + ip);
						callbackContext.success(ip);
					}catch (Exception e){
						Log.d(LOG_TAG, "cannot get IP address of " + __inf);
						callbackContext.error("cannot get IP address of " + __inf);
					}
					return;
				}
			});
			return true;
		}else if(action.equals("clearCache")){
			final String inf = args.getString(0);

			cordova.getThreadPool().execute(new Runnable() {
				@Override
				public void run() {
					try{
						Log.d(LOG_TAG, "try to delete cache");
						clearCache(cordova.getActivity());
						callbackContext.success("OK");
					}catch (Exception e){
						Log.d(LOG_TAG, "cannot delete cache: " + e.getMessage());
						callbackContext.error("cannot delete cache: " + e.getMessage());
					}
					return;
				}
			});
			return true;
		}else if(action.equals("readFile")){
			// write function to read file in java
			final String path = args.getString(0);
			cordova.getThreadPool().execute(new Runnable() {
				@Override
				public void run() {
					try {
						File file = new File(path);
						if(file.exists()) {
							FileInputStream fis = new FileInputStream(file);
							byte[] data = new byte[(int) file.length()];
							fis.read(data);
							fis.close();
							callbackContext.success(new String(data, "UTF-8"));
						}else{
							callbackContext.error("File not exists");
						}
					} catch (Exception e) {
						callbackContext.error("Error: " + e.getMessage());
					}
				}
			});
			return true;
		}else if(action.equals("readLine")){
			// write function to read file in java
			final String path = args.getString(0);
			cordova.getThreadPool().execute(new Runnable() {
				@Override
				public void run() {
					try {
						File file = new File(path);
						if(file.exists()) {
							BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
							String line = br.readLine();
							br.close();
							callbackContext.success(line);
						}else{
							callbackContext.error("File not exists");
						}
					} catch (Exception e) {
						callbackContext.error("Error: " + e.getMessage());
					}
				}
			});
			return true;
		}else if(action.equals("writeFile")){
			// write function to write file in java
			// https://stackoverflow.com/questions/14376807/how-to-write-string-to-file-in-android
			final String path = args.getString(0);
			final String data = args.getString(1);
			cordova.getThreadPool().execute(new Runnable() {
				@Override
				public void run() {
					try {
						File file = new File(path);
						FileOutputStream fos = new FileOutputStream(file);
						fos.write(data.getBytes());
						fos.close();
						callbackContext.success("OK");
					} catch (Exception e) {
						callbackContext.error("Error: " + e.getMessage());
					}
				}
			});
			return true;
		}else if(action.equals("openMonitor")){
			// open file, and occasionally read the file to check changes, if changes, return the content via callbackContext
			final JSONObject options = args.getJSONObject(0);
			final String path = options.getString("path");
			final Integer length = options.getInt("length");
			final Integer interval = options.getInt("interval");

			Log.d(LOG_TAG, "openMonitor() with options: " + options.toString());

			cordova.getThreadPool().execute(new Runnable() {
				@Override
				public void run() {
					try {
						File file = new File(path);
						if(file.exists()) {
							byte[] oldData = new byte[length];
							byte[] data = new byte[length];

							// read the file for every interval, if the content is changed, return the content via CallbackContext
							// if the content is not changed, do nothing
							// if the file is not exists, return error message
							// if the file is closed, return "OK"
							// exit if isMonitoring is false
							//

							while(isMonitoring) {
								//byte[] data = new byte[(int) file.length()];
								FileInputStream fis = new FileInputStream(file);
								fis.read(data, 0, length);
								fis.close();
								//Log.d(LOG_TAG, "openMonitor() data: " + new String(data, "UTF-8"));
								//Log.d(LOG_TAG, "openMonitor() oldData: " + new String(data, "UTF-8"));
								if(!java.util.Arrays.equals(data, oldData)){
								//if(!new String(data, "UTF-8").equals(new String(oldData, "UTF-8"))) {
									PluginResult result = new PluginResult(PluginResult.Status.OK, new String(data, "UTF-8"));
									result.setKeepCallback(true);
									callbackContext.sendPluginResult(result);
									//callbackContext.success(new String(data, "UTF-8"));
								
									// copy data of data to oldData
									System.arraycopy(data, 0, oldData, 0, length);
								}
								
								Thread.sleep(interval);
							}
							callbackContext.success("OK");
						}
					} catch (Exception e) {
						callbackContext.error("Error: " + e.getMessage());
					}
				}
			});
			return true;
		}else if(action.equals("closeMonitor")){
			isMonitoring = false;
			callbackContext.success("OK");
			return true;
		}else if(action.equals("setDepthOfViews")){
			// get view ids, and set the layer of the views
			// options: [{id: 1001, depth: 1}, {id: 1002, depth: 2}, ...]
			// id: 0 is the webview, and others are the view ids
			final JSONArray options = args.getJSONArray(0);
			Log.d(LOG_TAG, "setDepthOfViews() with options: " + options.toString());

			cordova.getThreadPool().execute(new Runnable() {
				@Override
				public void run() {
					try {
						for(int i=0; i<options.length(); i++) {
							JSONObject obj = options.getJSONObject(i);
							int id = obj.getInt("id");
							int depth = obj.getInt("depth");
							// set the layer of the view
							View view = null;
							if(id == 0) {
								view = webView.getView();
							} else {
								view = (View)cordova.getActivity().findViewById(id);
							}
							if(view != null) {
								Log.d(LOG_TAG, "setDepthOfViews() view: " + view.toString() + ", depth: " + depth);
								view.setZ(depth);
								//view.setBackgroundColor(Color.TRANSPARENT);
							}
						}
						callbackContext.success("setDepthOfViews() OK");
					} catch (Exception e) {
						callbackContext.error("Error: " + e.getMessage());
					}
				}
			});
			return true;

		}else if(action.equals("test")){
			final JSONObject options = args.getJSONObject(0);
			Log.d(LOG_TAG, "test: " + options.toString());
			/*
			PeripheralManager pm = PeripheralManager.getInstance();
			Log.d(LOG_TAG, "test: ");
			List<String> gpioList = pm.getGpioList();
			Log.d(LOG_TAG, "test: gpio list: " + gpioList.toString());
			*/

			try{
				Process process = null;
				DataOutputStream dataOutputStream = null;

				process = Runtime.getRuntime().exec("su");
				dataOutputStream = new DataOutputStream(process.getOutputStream());
				dataOutputStream.writeBytes("chmod 777 /sys/class/gpio/export\n");
				dataOutputStream.writeBytes("exit\n");
				dataOutputStream.flush();
				process.waitFor();
			}catch (IOException | InterruptedException e) {
				Log.i(LOG_TAG, "error: " + e.getMessage());
				callbackContext.error("error: " + e.getMessage());
			}

			callbackContext.success("OK");
			return true;
		}else if(action.equals("openSettings")){
			final String setting = args.getString(0);
			Log.d(LOG_TAG, "openSettings: " + setting);
			cordova.getThreadPool().execute(new Runnable() {
				@Override
				public void run() {
					try {
						if(setting.equals("settings")){
							openSettings();
						}else if(setting.equals("display")){
							openDisplaySettings();
						}else if(setting.equals("wifi")){
							openWifiSettings();
						}else if(setting.equals("bluetooth")){
							openBluetoothSettings();
						}else if(setting.equals("location")){
							openLocationSettings();
						}else if(setting.equals("security")){
							openSecuritySettings();
						}else if(setting.equals("application")){
							openApplicationSettings();
						}else if(setting.equals("battery")){
							openBatterySettings();
						}else if(setting.equals("datetime")){
							openDateTimeSettings();
						}else if(setting.equals("sound")){
							openSoundSettings();
						}else if(setting.equals("inputmethod")){
							openInputMethodSettings();
						}else if(setting.equals("deviceinfo")){
							openDeviceInfoSettings();
						}else{
							callbackContext.error("Invalid setting: " + setting);
						}
						callbackContext.success("OK");
					} catch (Exception e) {
						callbackContext.error("Error: " + e.getMessage());
					}
				}
			});
			return true;
		}

        return false;
    }

	private void execCommand(String command, CallbackContext callbackContext) {
		try {
			//Process process = Runtime.getRuntime().exec("/system/xbin/su 0 " + command);
			Process process = Runtime.getRuntime().exec(command);
			//Process process = Runtime.getRuntime().exec("ls -l /syst 2>&1");
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			BufferedReader error = new BufferedReader(new InputStreamReader(process.getErrorStream()));
			int read;
			char[] buffer = new char[4096];
			StringBuffer output = new StringBuffer();
			StringBuffer errMsg = new StringBuffer();

			while ((read = reader.read(buffer)) > 0) {
				output.append(buffer, 0, read);
				//Log.d(LOG_TAG, "reading: " + buffer.toString());
			}
			reader.close();

			while ((read = error.read(buffer)) > 0) {
				errMsg.append(buffer, 0, read);
				//Log.d(LOG_TAG, "reading: " + buffer.toString());
			}
			error.close();

			// Waits for the command to finish.
			int returnCode = process.waitFor();

			Log.i(LOG_TAG, "return code: " + returnCode + ", message: " + output.toString() + ", error: " + errMsg.toString());
			if(returnCode == 0) {
				callbackContext.success(output.toString());
			} else {
				callbackContext.error("Error code: " + returnCode + ", msg: " + errMsg.toString());
			}
		} catch (IOException | InterruptedException e) {
			//throw new RuntimeException(e);
			Log.i(LOG_TAG, "error: " + e.getMessage());
			callbackContext.error("error: " + e.getMessage());
		}
	}

	private void cmdShell(String command, CallbackContext callbackContext) {
		try {
			Shell.Result result = Shell.cmd(command).exec();
			List<String> out = result.getOut();
			int code = result.getCode(); // 0 means success
										 // 1 means failure
										 // 2 means no root
										 // 3 means no busybox
										 // 4 means command not found
										 // 5 means permission denied
										 // 6 means timeout
										 // 7 means interrupted
										 // 8 means other error
										 // 9 means no shell
										 // 10 means shell closed
										 // 11 means shell not initialized
										 // 12 means shell is busy
										 // 13 means shell is dead
										 // 14 means no output
										 // 15 means output too large
										 // 16 means output truncated
										 // 126 means command cannot execute
										 // 255 means unknown error
										 // -1 means not executed

			if(code == 0) {
				callbackContext.success(new JSONArray(out));
			} else {
				callbackContext.error("Error code: " + code);
			}
		} catch (Exception e) {
			Log.i(LOG_TAG, "error: " + e.getMessage());
			callbackContext.error("error: " + e.getMessage());
		}
	}

	private void suShell(String command, CallbackContext callbackContext) {
		try {
			Shell.Result result = Shell.su(command).exec();
			List<String> out = result.getOut();
			int code = result.getCode(); 
			if(code == 0) {
				callbackContext.success(new JSONArray(out));
			} else {
				callbackContext.error("Error code: " + code);
			}
		} catch (Exception e) {
			Log.i(LOG_TAG, "error: " + e.getMessage());
			callbackContext.error("error: " + e.getMessage());
		}
	}

	// get MAC Address & IP Address
	// https://smartstore.naver.com/xiaomistory/products/7938899469?NaPm=ct%3Dli19d6b7%7Cci%3Dshopn%7Ctr%3Ddana%7Chk%3D7558d59fa6bba9db5ba370b47ac3e2ed52330d94%7Ctrx%3Dundefined

	/**
	 * Returns MAC address of the given interface name.
	 * @param interfaceName eth0, wlan0 or NULL=use first interface 
	 * @return  mac address or empty string
	 */
	private static String getMacAddress(String interfaceName) {
		try {
			List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
			for (NetworkInterface intf : interfaces) {
				Log.i(LOG_TAG, "getMacAddress intf name: " + interfaceName + ":" + intf.getName());
				if (interfaceName != null) {
					if (!intf.getName().equalsIgnoreCase(interfaceName)) continue;
				}
				byte[] mac = intf.getHardwareAddress();
				if (mac==null) return "";
				StringBuilder buf = new StringBuilder();
				for (byte aMac : mac) buf.append(String.format("%02x:",aMac));  
				if (buf.length()>0) buf.deleteCharAt(buf.length()-1);
				return buf.toString();
			}
		} catch (Exception e) {
			Log.i(LOG_TAG, "getMacAddress error: " + e.getMessage());
		} // for now eat exceptions
		return "";
	}

	/**
	 * Get IP address from first non-localhost interface
	 * @param useIPv4   true=return ipv4, false=return ipv6
	 * @return  address or empty string
	 */
	private static String getFirstIpAddress() {
		return getFirstIpAddress(true);
	}

	private static String getFirstIpAddress(boolean useIPv4) {
		try {
			List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
			for (NetworkInterface intf : interfaces) {
				List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
				for (InetAddress addr : addrs) {
					if (!addr.isLoopbackAddress()) {
						String sAddr = addr.getHostAddress();
						//boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
						boolean isIPv4 = sAddr.indexOf(':')<0;

						if (useIPv4) {
							if (isIPv4) 
								return sAddr;
						} else {
							if (!isIPv4) {
								int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
								return delim<0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
							}
						}
					}
				}
			}
		} catch (Exception ignored) { } // for now eat exceptions
		return "";
	}

	/**
	 * Get IP address of given interface name.
	 * @param interfaceName eth0, wlan0 or NULL=use first interface 
	 * @return  mac address or empty string
	 */

	private static String getIPv4Address(String interfaceName){
		try {
			List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
			for (NetworkInterface intf : interfaces) {
				if (interfaceName != null) {
					if (!intf.getName().equalsIgnoreCase(interfaceName)) continue;
				}
				List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
				for (InetAddress addr : addrs) {
					if (!addr.isLoopbackAddress()) {
						String sAddr = addr.getHostAddress();
						//boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
						boolean isIPv4 = sAddr.indexOf(':')<0;
						if (isIPv4) return sAddr;
					}
				}
			}
		} catch (Exception e) {
			Log.i(LOG_TAG, "getIPv4Address error: " + e.getMessage());
		} // for now eat exceptions
		return "";
	}

	// delete cache directory
	private static void clearCache(Context context) {
		try {
			File dir = context.getCacheDir();
			deleteDir(dir);
		} catch (Exception e) { e.printStackTrace();}
	}

	private static boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
			return dir.delete();
		} else if(dir!= null && dir.isFile()) {
			return dir.delete();
		} else {
			return false;
		}
	}

	private void setAudioVolume(int volumePercent) {
		// 1. get max volume from AudioManager
		// 2. calculate the volume from the max volume & volumePercent
		// 3. set the volume to AudioManager
		AudioManager audioManager = (AudioManager)cordova.getActivity().getSystemService(Context.AUDIO_SERVICE);
		int maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		int volume = (int)(maxVolume * volumePercent / 100);
		audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);
	}

	// open Settings
	private void openSettings() {
		Intent intent = new Intent(Settings.ACTION_SETTINGS);
		cordova.getActivity().startActivity(intent);
	}

	private void openDisplaySettings() {
		Intent intent = new Intent(Settings.ACTION_DISPLAY_SETTINGS);
		cordova.getActivity().startActivity(intent);
	}

	private void openWifiSettings() {
		Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
		cordova.getActivity().startActivity(intent);
	}

	private void openBluetoothSettings() {
		Intent intent = new Intent(Settings.ACTION_BLUETOOTH_SETTINGS);
		cordova.getActivity().startActivity(intent);
	}

	private void openLocationSettings() {
		Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		cordova.getActivity().startActivity(intent);
	}

	private void openSecuritySettings() {
		Intent intent = new Intent(Settings.ACTION_SECURITY_SETTINGS);
		cordova.getActivity().startActivity(intent);
	}

	private void openApplicationSettings() {
		Intent intent = new Intent(Settings.ACTION_APPLICATION_SETTINGS);
		cordova.getActivity().startActivity(intent);
	}

	private void openBatterySettings() {
		Intent intent = new Intent(Intent.ACTION_POWER_USAGE_SUMMARY);
		cordova.getActivity().startActivity(intent);
	}

	private void openDateTimeSettings() {
		Intent intent = new Intent(Settings.ACTION_DATE_SETTINGS);
		cordova.getActivity().startActivity(intent);
	}

	private void openSoundSettings() {
		Intent intent = new Intent(Settings.ACTION_SOUND_SETTINGS);
		cordova.getActivity().startActivity(intent);
	}

	private void openInputMethodSettings() {
		Intent intent = new Intent(Settings.ACTION_INPUT_METHOD_SETTINGS);
		cordova.getActivity().startActivity(intent);
	}

	private void openDeviceInfoSettings() {
		Intent intent = new Intent(Settings.ACTION_DEVICE_INFO_SETTINGS);
		cordova.getActivity().startActivity(intent);
	}

}
