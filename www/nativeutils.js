const exec = require("cordova/exec");

module.exports = {
	fileExists: function(path, successCallback, errorCallback) {
		exec(successCallback, errorCallback, "NativeUtils", "fileExists", [path]);
	},
	exitApp: function(path, successCallback, errorCallback) {
		exec(successCallback, errorCallback, "NativeUtils", "exitApp", []);
	},
	setMinimumFontSize: function(val, successCallback, errorCallback) {
		console.log('NativeUtils setMinimumFontSize: ' + val)
		exec(successCallback, errorCallback, "NativeUtils", "setMinimumFontSize", [val]);
	},
	audioOn: function(successCallback, errorCallback) {
		exec(successCallback, errorCallback, "NativeUtils", "audioOn", []);
	},
	audioOff: function(successCallback, errorCallback) {
		exec(successCallback, errorCallback, "NativeUtils", "audioOff", []);
	},
	suRun: function(command, successCallback, errorCallback) {
		exec(successCallback, errorCallback, "NativeUtils", "suRun", [command])
	},
	cmdRun: function(command, successCallback, errorCallback) {
		exec(successCallback, errorCallback, "NativeUtils", "cmdRun", [command])
	},
	getMacAddress: function(inf, successCallback, errorCallback) {
		exec(successCallback, errorCallback, "NativeUtils", "getMacAddress", [inf])
	},
	getIpAddress: function(inf, successCallback, errorCallback) {
		exec(successCallback, errorCallback, "NativeUtils", "getIpAddress", [inf])
	},
	clearCache: function(successCallback, errorCallback) {
		exec(successCallback, errorCallback, "NativeUtils", "clearCache", [])
	},
	readFile: function(path, successCallback, errorCallback) {
		exec(successCallback, errorCallback, "NativeUtils", "readFile", [path])
	},
	readLine: function(path, successCallback, errorCallback) {
		exec(successCallback, errorCallback, "NativeUtils", "readLine", [path])
	},
	writeFile: function(path, data, successCallback, errorCallback) {
		exec(successCallback, errorCallback, "NativeUtils", "writeFile", [path, data])
	},
	openMonitor: function(opts, successCallback, errorCallback) {
		exec(successCallback, errorCallback, "NativeUtils", "openMonitor", [opts])
	},
	closeMonitor: function(successCallback, errorCallback) {
		exec(successCallback, errorCallback, "NativeUtils", "closeMonitor", [])
	},
	setDepthOfViews: function(opts, successCallback, errorCallback) {
		exec(successCallback, errorCallback, "NativeUtils", "setDepthOfViews", [opts])
	},
	setAudioVolume: function(opts, successCallback, errorCallback) {
		exec(successCallback, errorCallback, "NativeUtils", "setAudioVolume", [opts])
	},
	test: function(opts, successCallback, errorCallback) {
		exec(successCallback, errorCallback, "NativeUtils", "test", [opts])
	},
	openSettings: function(setting, successCallback, errorCallback) {
		exec(successCallback, errorCallback, "NativeUtils", "openSettings", [setting])
	}
};
